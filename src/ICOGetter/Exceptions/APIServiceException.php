<?php

namespace App\ICOGetter\Exceptions;


class APIServiceException extends \Exception {


    public function __construct(string $message, int $code)
    {
        parent::__construct("Error: " . $message, $code);
    }


}