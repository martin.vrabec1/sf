<?php

namespace App\ICOGetter;

use App\ICOGetter\Exceptions\APIServiceException;
use App\ICOGetter\Services\ICOService;

/**
 * This is main class for 3. example, will inject ICO service which call ico api
 */
class ICOGetter
{

    private ICOService $icoService;

    public function __construct(ICOService $icoService)
    {
        $this->icoService = $icoService;
    }

    /**
     * Main code to get ICO, getICO method from service
     * is processing here and response is returned
     */
    public function getICO(string $ico)
    {
        try {
            $response = $this->icoService->getICO($ico);

            return $response;
        } catch (APIServiceException $ex) {
            if ($ex->getCode() === 404) {
                echo sprintf("ICO nenajdene");
            }
        } catch (\Exception $ex) {
            echo sprintf('Nastala chyba pri ziskavani ICO');
        }
    }
}
