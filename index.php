<?php

use App\ICOGetter\ICOGetter;
use App\ICOGetter\Services\ICOService;
use App\IterationResult;

require_once __DIR__.'/vendor/autoload.php';

//1. example
IterationResult::runCode();

//3.
$icoService = new ICOService();
$icoGetter = new ICOGetter($icoService);
var_dump($icoGetter->getICO('115'));
var_dump($icoGetter->getICO('01569651'));
var_dump($icoGetter->getICO('00000000'));