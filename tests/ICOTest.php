<?php

use App\ICOGetter\Exceptions\InputException;
use App\ICOGetter\Services\ICOService;
use PHPUnit\Framework\TestCase;

class ICOTest extends TestCase
{

    private $icoService;

    public function setUp(): void
    {
        $this->icoService = new ICOService();
    }

    public function testICO(): void
    {
        $this->expectException(Exception::class);
        $this->icoService->getICO('00000000');
    }

    public function testICO2(): void
    {
        $this->expectException(InputException::class);
        $this->icoService->getICO('112');
    }
}
