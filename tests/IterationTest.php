<?php

use App\IterationResult;
use PHPUnit\Framework\TestCase;

class IterationTest extends TestCase {

    public function testSFIteration() :void
    {
        //15
        $this->assertEquals('SuperFaktura', IterationResult::getIterationResult(15));

        //12
        $this->assertEquals('Super', IterationResult::getIterationResult(12));

        //10
        $this->assertEquals('Faktura', IterationResult::getIterationResult(10));

        //5
        $this->assertEquals('Faktura', IterationResult::getIterationResult(5));

        //98
        $this->assertEquals('98', IterationResult::getIterationResult(98));
    }


}