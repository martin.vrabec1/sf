<?php

namespace App\ICOGetter\Services;

/**
 * This class act as a singleton to get instance of guzzle client
 */
class GuzzleClient {

    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new \GuzzleHttp\Client();
        }
        
        return self::$instance;
    }

    public function __clone()
    {
        
    }

}