<?php

namespace App\ICOGetter\Services;

use App\ICOGetter\Exceptions\APIServiceException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use PhpParser\Node\Expr\Cast\Object_;
use Psr\Http\Message\ResponseInterface;
use stdClass;

class ApiService
{

    /**
     * Call REST method on remote server
     * @param string method method to call on server [GET, POST, ...]
     * @param string url to call on server
     * @param array options data to send on server 
     * @param json json attribute
     * @return string|null response body
     * @throws APIServiceException
     */
    public function doRequest(string $method, string $url, $options = [], $json = true): string|null|Object
    {
        try {
            $response = $this->getResponse($method, $url, $options);

            $string = (string)$response->getBody();
            return $json ? $string : json_decode($string);
        } catch (\Exception $ex) {
            $code = 0;
            if ($ex instanceof GuzzleException) {
                $message = $ex->getMessage();
            }
            if ($ex instanceof ClientException) {
                $message = $ex->getResponse();
                $code = $ex->getCode();
            }
            if ($ex instanceof RequestException) {
                $message = $ex->getMessage();
            }
            throw new APIServiceException($message, $code);
        }
    }

    /**
     * Send request to server and return guzzle response
     */
    public function getResponse($method, $url): ResponseInterface
    {
        $client = GuzzleClient::getInstance();
        $response = $client->request($method, $url);

        return $response;
    }
}
