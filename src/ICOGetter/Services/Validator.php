<?php

namespace App\ICOGetter\Services;

class Validator {

    /**
     * Validate ICO
     * @return bool
     */
    public static function validateICO(string $ico) :bool
    {
        if (!preg_match('/[0-9]{8}/', $ico)) {
            return false;
        }

        return true;
    
    }


}