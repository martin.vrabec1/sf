*****Riesenie 1. aj 3. v kode:*****

Vytvoril som to tak, ze index.php obsahuje uz spustane vystupy:

1. iteracia je vyriesena v subore v IterationResult.php kde su metody runCode() v ktorej sa vytvara cyklus a getIterationResult() ktora vrati string, ktory sa ma vypisat

3. obsahuje singleton ktory vrati instanciu GuzzleHttp, requesty su robene v abstraktnej triede ApiService, na ico je vytvorena trieda ICOService, ktora spusta request na samotnu url, input by sa zadaval v triede ICOGetter a metode getICO(), kde su aj spracovane vynimky
samostatnymi triedami su validator a aj testy

*****ZADANIE*****

* 2. databázová. *

Máte jednoduchú tabuľku s primárnym kľúčom a hodnotou v druhom stĺpci. Niektoré z týchto hodnôt môžu byť duplicitné. Napíšte prosím SQL query, ktorá vráti všetky riadky z tabuľky s duplicitnými hodnotami (*celé* riadky).

Definícia tabuľky a vzorové dáta:
CREATE TABLE `duplicates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `duplicates` (`id`, `value`) VALUES
(1,    1),
(2,    2),
(3,    3),
(4,    2),
(5,    4),
(6,    4),
(7,    5),
(8,    6),
(9,    6),
(10,    2);

Želaný výstup:

+----+-------+
| id | value |
+----+-------+
|  2 |     2 |
|  4 |     2 |
|  5 |     4 |
|  6 |     4 |
|  8 |     6 |
|  9 |     6 |
| 10 |     2 |
+----+-------+

Bude vaše riešenie efektívne fungovať aj na tabuľke s veľkým počtom riadkov (milión a viac)? Vysvetlite prečo a ako.

***** RIESENIE *****

SELECT * FROM duplicates WHERE value IN (SELECT value FROM duplicates GROUP BY value HAVING count(value) >= 2);

Fungovat by mohlo v pripade dostatocneho vykonu mysql servera, trvat by to ale mohlo dlho. Zrychlenie by bolo mozne pridanim indexu na value cim by sa optimalizovalo citanie a triedenie v tabulke, zaroven by sa spomalilo zapisovanie do tabulky
