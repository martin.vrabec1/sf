<?php

namespace App;

/**
 * First example ->iteration, this class 
 */
class IterationResult
{

    /**
     * Iteration code running
     */
    public static function runCode(): void
    {
        for ($i = 1; $i <= 100; $i++) {
            echo sprintf("%s\n", self::getIterationResult($i));
        }
    }

    /**
     * Return string or number according to input number
     * @param int number
     * @return string|int
     */
    public static function getIterationResult(int $number): string|int
    {
        if ($number % 15 === 0) {
            return 'SuperFaktura';
        }

        if ($number % 5 === 0) {
            return 'Faktura';
        }

        if ($number % 3 === 0) {
            return 'Super';
        }

        return $number;
    }
}
