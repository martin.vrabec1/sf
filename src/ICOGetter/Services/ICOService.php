<?php

namespace App\ICOGetter\Services;

use App\ICOGetter\Exceptions\InputException;
use Exception;

/**
 * Service class for getting ICO from endpoint with requested ico number
 */
class ICOService extends ApiService
{

    const URL = 'https://ares.gov.cz/ekonomicke-subjekty-v-be/rest/ekonomicke-subjekty/';

    public function getICO(string $ico)
    {
        //validate
        $valid = Validator::validateICO($ico);

        if (!$valid) {
            throw new InputException("ICO nie je v spravnom formate");
        }

        $url = sprintf(self::URL . '%08d', $ico);

        $response = $this->doRequest('GET', $url);

        return $response;
    }
}
